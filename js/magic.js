/*SMOOTH SCROLL*/

$(document).ready(function(){

	$('a[href*=#]').on('click', function(event){     
	    event.preventDefault();
	    $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
	});
});

/*SHOW SCREEN SIZE*/

$(document).ready(function(){
	$(window).on('load', showSize);
	$(window).on('resize', showSize);
	showSize();
	function showSize() {
	$('#width').html(/*'HEIGHT:'+$(window).height()+*/' WIDTH '+$(window).width()+'px');
	}
});

/*DIALOG BOX*/

$(document).ready(function() {
	$('.dialog').hide();

    $('.open-dialog').click(function() {
    	$('.container').addClass('dimmed');
        $('.overlay').css('display', 'block');
        $('.dialog').fadeToggle(250);
    });

    $('.close-dialog').click(function() {
    	$('.container').removeClass('dimmed');
       	$('.overlay').css('display', 'none');
       	$('.dialog').toggle();
    });
});

