---
layout: post
title:  "Sending files to clients"
date:   2014-09-14 09:53:09
categories: checklist
---

### File formatting

* <input type="checkbox"> Are you using the right template? (L:/Internal/templates)
* <input type="checkbox"> Check type is *set in Arial*
* <input type="checkbox"> Spell check!
* <input type="checkbox"> Is this something the client should not be able to modify? Then PDF it

### Sending the email

* <input type="checkbox"> Explain what the file is
* <input type="checkbox"> Detail what kind of feedback you're looking for, and _when_ are you expecting it