---
layout: post
title:  "Stakeholder workshop"
date:   2014-09-14 09:53:09
categories: checklist workshop
---

### Preparation

* <input type="checkbox"> Send workshop agenda with tentative timings
* <input type="checkbox"> List of atendees
* <input type="checkbox"> Directions / when are you going to leave the office

Your workshop box should contain:

* <input type="checkbox"> Brown paper
* <input type="checkbox"> Scissors
* <input type="checkbox"> Sticky notes (tons)
* <input type="checkbox"> Voting dots
* <input type="checkbox"> Drafting dots
* <input type="checkbox"> Sharpies (for the stakeholders + consultants + 2/3 extra)

### Running the workshop

1. Take note of who's sitting where, so you remember the names
2. If you're not capturing, the conversation is useless -> Move on